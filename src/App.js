import logo from './logo.svg';
import './App.css';
import Order from './pages/order/order';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import OrderHistory from './pages/order-history/order-history';


function App() {
  return (
    <Router>
      <Switch>
        <Route path="/order">
          <Order />
        </Route>
        <Route path="/order-history">
          <OrderHistory />
        </Route>
        <Route path="/">
            <OrderHistory />
          </Route>
      </Switch>
    </Router >

  );
}

export default App;
