
import React, { useEffect, useState } from 'react';
import { Container, Form, Table, Button, Row, Col } from 'react-bootstrap';

const OrderHistory = () => {

    const history = localStorage.getItem('orderHistory');
    const initialOrderValues = JSON.parse(history);
    const [orderHistory, setOrderHistory] = useState(initialOrderValues);


    const filter = (name) => {
        const filteredOrders = initialOrderValues.filter(el => el.name.startsWith(name));
        setOrderHistory(filteredOrders);
    }



    return <>
        <Container>
            <h3>Order History</h3>
            <Form>
                <Row>
                    <Col>
                        <Form.Group controlId="name">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" placeholder="Name" onChange={(e) => filter(e.target.value)}  />
                        </Form.Group>
                    </Col>
                </Row>
            </Form>

            <Table striped bordered hover className='mt-2'>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Buns</th>
                        <th>Salad</th>
                        <th>Cheese Slice</th>
                        <th>Cutlets</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        orderHistory.map((order, index) =>
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{order.name}</td>
                                <td>{order.buns}</td>
                                <td>{order.salad ? 'Yes' : 'No'}</td>
                                <td>{order.cheeseSlice}</td>
                                <td>{order.cutlets}</td>
                                <td>${order.price}</td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>

        </Container>
    </>
}

export default OrderHistory;