
import React, { useEffect, useState } from 'react';
import { Col, Form, Row, Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import './order.css';

const Order = () => {

    const [name, setName] = useState();
    const [buns] = useState(2);
    const [salad, setSalad] = useState(false);
    const [cheeseSlice, setCheeseSlice] = useState();
    const [cutlets, setCutlets] = useState();
    const [price, setPrice] = useState(0);

    const history = useHistory();

    const onSubmit = () => {  
        const orderDetails = {name,buns,salad,cheeseSlice,cutlets,price};
        const orderHistory = JSON.parse(localStorage.getItem('orderHistory') || '[]');
        orderHistory.push(orderDetails);
        localStorage.setItem('orderHistory',JSON.stringify(orderHistory));
        history.push('/order-history');
    }

    useEffect(() => {
        let newPrice = buns * 5;

        if (salad)
            newPrice += 5;

        if (cutlets) {
            newPrice += 2 * cutlets;

        }
        if (cheeseSlice) {
            newPrice += 1 * cheeseSlice;
        }

        setPrice(newPrice);
       
    }, [salad, cutlets, cheeseSlice, buns]);

    return <div className="order-container">
        <Row>
            <Col>
                <h3>Order your Burger</h3>
                <Form onSubmit={onSubmit}>
                    <Row>
                        <Col>
                            <Form.Group controlId="name">
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" placeholder="Name" onBlur={(e) => setName(e.target.value)} value={name} />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <Form.Group controlId="buns">
                                <Form.Label>Buns</Form.Label>
                                <Form.Control type="number" placeholder="Buns" value={buns} readOnly />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <Form.Group controlId="salad">
                                <Form.Check type="checkbox" label="Salad" onClick={(e) => setSalad(e.target.checked)} value={salad} />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <Form.Group controlId="cheeseSlices">
                                <Form.Label>Cheese Slices</Form.Label>
                                <Form.Control type="number" placeholder="Cheese Slice" onBlur={(e) => setCheeseSlice(e.target.value)} value={cheeseSlice} />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <Form.Group controlId="cutlets">
                                <Form.Label>Cutlets</Form.Label>
                                <Form.Control type="number" placeholder="Cutlets" onBlur={(e) => setCutlets(e.target.value)} value={cutlets} />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <h6> Price : ${price}</h6>
                        </Col>
                    </Row>
                    <Row className='mt-2'>
                        <Col>
                            <Button variant="primary" type="button" onClick={onSubmit}>
                                Submit </Button>
                        </Col>
                    </Row>
                </Form>
            </Col>

        </Row>
    </div>
};

export default Order;